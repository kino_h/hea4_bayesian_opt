#!/usr/bin/env python
# coding: utf-8

# In[95]:


#!/usr/bin/env python
# coding: utf-8

# In[150]:
import warnings
import copy
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.linear_model import BayesianRidge, LinearRegression
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from scipy.stats import multivariate_normal
import random
import pandas as pd
from multi_object_scalization import ScalizeTarget
import sys
sys.path.append(".")


print(__doc__)

g_figsize = (5, 3)

# 2 targets

functype = 4

if functype == 1:
    def func(x):
        y = x
        z1, z2 = np.sin(2*np.pi*x), np.cos(2*np.pi*y)
        zall = np.vstack([z1, z2]).T
        return zall
    g_target_region = [[0.35, 0.45], [0.1, 0.2]]
    g_scale = [0.1, 0.3]

elif functype == 2:
    def func(x):
        y = x
        z1, z2 = np.sin(2*np.pi*x) + 0.3*np.cos(5*np.pi *
                                                x)**2, np.cos(2*np.pi*y)+y**2,
        zall = np.vstack([z1, z2]).T
        return zall

    g_target_region = [[0.5, 2], [0.1, 0.5]]
    g_scale = [0.1, 0.3]

elif functype == 3:
    def func(x):
        y = x
        z1, z2 = np.sin(2*np.pi*x) + 0.3*np.cos(5*np.pi *
                                                x)**2, np.cos(2*np.pi*y)+y**2,
        zall = np.vstack([z1, z2]).T
        return zall

    g_target_region = [[-2, -0.5], [0.1, 0.5]]
    g_scale = [0.1, 0.3]

elif functype == 4:
    def func(x):
        y = x
        z1, z2 = np.sin(2*np.pi*x) + 0.3*np.cos(5*np.pi *
                                                x)**2, np.cos(2*np.pi*y)+y**2,
        zall = np.vstack([z1, z2]).T
        return zall

    g_target_region = [[0, 0.5], [0.7, 1.2]]
    g_scale = [0.1, 0.3]

g_scalizedtarget = ScalizeTarget(g_target_region, g_scale)


def plot_xy(df, df_metadata, figsize=g_figsize):
    """plot x vs y. data is from df

    Args:
        df (pd.DataFrame): data
        df_metadata (dict): metadata of data
        figsize (tuple, optional): figure size. Defaults to g_figsize.
    """
    feature_column, target_columns = df_metadata["feature_columns"], df_metadata["target_columns"]
    tr_target_columns, sc_target_column = df_metadata[
        "normalized_target_columns"], df_metadata["scalized_target"]

    x = df.loc[:, feature_column].values.reshape(-1)

    zall = df.loc[:, target_columns].values
    ta = df.loc[:, sc_target_column].values
    v = df.loc[:, tr_target_columns].values
    print("ta.shape", ta.shape)

    n = zall.shape[1]

    xlim = [x.min(), x.max()]
    fig = plt.figure(figsize=figsize, facecolor="w")
    for i in range(n):
        ax = fig.add_subplot(1, n, i+1)
        ax.plot(x, zall[:, i], label=target_columns[i])
        ax.fill_between(xlim, [g_target_region[i][0], g_target_region[i][0]],
                        [g_target_region[i][1], g_target_region[i][1]], alpha=0.2)
    fig.legend()
    fig.show()

    print(zall.shape)

    fig = plt.figure(figsize=figsize, facecolor="w")

    for i, label in enumerate(target_columns):
        ax = fig.add_subplot(1, len(target_columns), i+1)
        ax.plot(x, v[:, i], label="transformed_{}".format(target_columns[i]))

    fig.legend()
    fig.show()

    fig = plt.figure(figsize=figsize, facecolor="w")
    ax = fig.add_subplot(111)
    for i, label in enumerate(target_columns):
        ax.plot(x, v[:, i], label="transformed_{}".format(target_columns[i]))

    ax.plot(x, ta, label="scaliezed target")
    ax.legend()
    fig.show()


def plot_targetspace(df, df_metadata, target_region):
    """plot targets

    Args:
        df (pd.DataFrame): data
        df_metadata (dict): metadata
        target_region (list): target region
    """
    target_columns, sc_target_column = df_metadata["target_columns"], df_metadata["scalized_target"]

    zall = df.loc[:, target_columns].values
    ta = df.loc[:, sc_target_column].values

    fig, ax = plt.subplots(figsize=g_figsize)

    xy = [target_region[0][0], target_region[1][0]]
    width = target_region[0][1]-target_region[0][0]
    height = target_region[1][1]-target_region[1][0]
    rect = Rectangle(xy, width, height, alpha=0.2)
    ax.add_patch(rect)

    img = ax.scatter(zall[:, 0], zall[:, 1], c=ta)
    plt.colorbar(img)
    i = ta == 0
    ax.scatter(zall[i, 0], zall[i, 1], c="red")
    ax.set_xlabel(target_columns[0])
    ax.set_ylabel(target_columns[1])
    ax.legend()
    fig.tight_layout()
    fig.show()


def plot_func(f):
    """test run of f

    Args:
        f (func): function
    """
    x = np.linspace(0., 1., 100)
    zall = f(x)
    scalizetarget = ScalizeTarget(g_target_region, g_scale)
    ta, v = scalizetarget.apply(zall)

    feature_list = ["feature0"]
    target_list = ["target0", "target1"]
    dffeature = pd.DataFrame(x, columns=feature_list)
    dftarget = pd.DataFrame(zall, columns=target_list)
    df = pd.concat([dffeature, dftarget], axis=1)

    sc_target_column = "scaliezed_target"
    dfsctarget = pd.DataFrame(ta, columns=[sc_target_column])
    tr_target_columns = []
    for target in target_list:
        tr_target_columns.append("transformed_{}".format(target))
    dftrtarget = pd.DataFrame(v, columns=tr_target_columns)

    df = pd.concat([dffeature, dftarget, dftrtarget, dfsctarget], axis=1)

    df_metadata = {"feature_columns": feature_list, "target_columns": target_list,
                   "scalized_target": sc_target_column, "normalized_target_columns": tr_target_columns}

    # plot_xy(df, df_metadata)
    plot_targetspace(df, df_metadata, g_target_region)


if True:
    plot_func(func)

# In[96]:


seed = 123
np.random.RandomState(seed)
np.random.seed(seed)


def make_all_data(n_order=3):
    """make all the data

    Args:
        n_order (int, optional): order of vander function. Defaults to 3.

    Returns:
        pd.DataFrame: data
        dict: metadata
    """

    df_metadata = {}

    x_test = np.linspace(0., 1., 100)
    y_test = func(x_test)

    X_test = np.vander(x_test, n_order + 1, increasing=True)
    X_test = X_test[:, 1:]

    # 1
    target_columns = []
    for i in range(y_test.shape[1]):
        target_columns.append("target{}".format(i))
    dftarget = pd.DataFrame(y_test, columns=target_columns)
    df_metadata.update({"target_columns": target_columns})

    # 2
    feature_columns = []
    for i in range(X_test.shape[1]):
        feature_columns.append("feature{}".format(i))
    dffeature = pd.DataFrame(X_test, columns=feature_columns)
    df_metadata.update({"feature_columns": feature_columns})

    # 3
    normalized_feature_columns = []
    for feature in feature_columns:
        normalized_feature_columns.append("normalized_{}".format(feature))
    scaler = StandardScaler()
    normalized_x = scaler.fit_transform(dffeature.values)
    dfnormalizedfeature = pd.DataFrame(
        normalized_x, columns=normalized_feature_columns)
    df_metadata.update({"normalized_feature_columns": normalized_feature_columns})

    # fill in df later
    normalized_target_columns = []
    for target in target_columns:
        normalized_target_columns.append("normalized_{}".format(target))
    df_metadata.update(
        {"normalized_target_columns": normalized_target_columns})

    predicted_target_columns = []
    predicted_std_target_columns = []
    for target in target_columns:
        predicted_target_columns.append("predicted_{}".format(target))
        predicted_std_target_columns.append("predicted_std_{}".format(target))
    df_metadata.update({"predicted_target_columns": predicted_target_columns})
    df_metadata.update(
        {"predicted_std_target_columns": predicted_std_target_columns})

    df = pd.concat([dftarget, dffeature, dfnormalizedfeature], axis=1)

    return df, df_metadata


g_df, g_df_metadata = make_all_data(n_order=3)

g_df.to_csv("simple.csv")
import json
with open("simple_metadata.json","w") as f:
    json.dump(g_df_metadata, f)
raise

# In[97]:


def test_regression(df, df_metadata):
    """regression test

    Args:
        df (pd.DataFrame): data
        df_metadata (dict): metadata
    """
    target_columns, feature_columns = df_metadata["target_columns"], df_metadata["feature_columns"]

    for target_name in target_columns:
        reg = LinearRegression(fit_intercept=True)
        X_test = df.loc[:, feature_columns].values
        z_test = df.loc[:, target_name].values
        reg.fit(X_test, z_test)
        score = reg.score(X_test, z_test)
        print(target_name, "score", score)


test_regression(g_df, g_df_metadata)


# In[98]:


def plot_train_test_fill(x_train, z_all,
                         z_train,
                         zpmean, zpstd,
                         title, axes):
    """fill plot  for training and test data

    Args:
        x_train (np.array): feature data for the x axis
        z_all (np.array): all the target data
        z_train (np.array): training data
        zpmean (np.array): predicted mean values
        zpstd (np.array): predicted stddev values
        title (str): title of the figure
        axes (np.array): matplotlib.axes
    """
    x_all = np.array(list(range(z_all.shape[0])))

    n_target = z_all.shape[1]

    for i in range(n_target):

        ax = axes[i]
        ax.fill_between(x_all, zpmean[:, i]-zpstd[:, i],
                        zpmean[:, i]+zpstd[:, i],
                        color="pink", alpha=0.5, label="$y_{predict std}$")
        ax.plot(x_all, zpmean[:, i], color="red",
                lw=2, label="$y_{predict mean}$")

        ax.fill_between([x_all.min(), x_all.max()],
                        [g_target_region[0][0], g_target_region[0][0]],
                        [g_target_region[0][1], g_target_region[0][1]],
                        "--", color="green", alpha=0.2)

        ax.plot(x_all, z_all[:, i], color="blue", lw=2, label="$y_{expr}$")

        ax.scatter(x_train, z_train[:, i], s=50, alpha=1, label="$y_{train}$")
        if i == 0:
            ax.set_title(title)
        ax.set_ylabel('y')
        ax.legend(bbox_to_anchor=(0.5, -0.1),
                  loc='upper center', borderaxespad=0)


# In[99]:


def plot_range(acq, y_p_mean, y_std, index, ax):
    """plot ymean and ystd

    Args:
        acq (np.array): aquisition function values
        y_p_mean (np.array): predicted mean values
        y_std (np.array): predicted stddev values
        index (np.array): actions
        ax (matplitlib.axes): axies
    """

    x = range(y_p_mean.shape[0])
    ax.fill_between(x, y_p_mean.ravel()-y_std,
                    y_p_mean.ravel()+y_std,
                    color="pink", alpha=0.5, label="$y_{predict std}$")
    ax.plot(x, y_p_mean.ravel(), color='blue')
    ax.scatter(index, y_p_mean.ravel()[index],
               color='red', label="mean")
    ax.plot(x, acq.ravel(),
            color='green', label="acquistition function")
    ax.scatter(index, acq.ravel()[index],
               color='green', label="new selection")
    ax.legend(bbox_to_anchor=(0.5, -0.1), loc='upper center', borderaxespad=0)


# In[100]:


class BayesianRidgeOpt:
    """Bayesian optimiation using Bayesian Ridge regression
    """

    def __init__(self, df, df_metadata, scalizetarget, method=None, lambda_=None):
        """initialization

        Args:
            df (pd.DataFram): data
            df_metadata (dict): metadata
            scalizetarget (func): function for scalizedtarget
            method (dict, optional): dict of acqusition funciton and optimization. Defaults to None.
            lambda_ (float, optional): initial lambda value of Bayesian Ridge regression. Defaults to None.
        """
        self.df = df.copy()
        self.df_metadata = copy.deepcopy(df_metadata)

        self.scalizetarget = scalizetarget

        _method = {'acquisition_function': 'TS', 'optimize': 'min'}
        if method is None:
            self.method = _method
        else:
            _method.update(method)
            self.method = _method

        self.lambda_ = lambda_

    def _fit_predict(self, actions, target_column, alpha=None, lambda_=None):
        """fit and predict

        Args:
            actions (np.array): a list of actions
            target_column (str): target column name
            alpha (float, optional): alpha value of Bayesian Ridge regression. Defaults to None.
            lambda_ (float, optional): lambda vlaue of Bayesian Ridge regression. Defaults to None.

        Returns:
            [type]: [description]
        """
        reg = BayesianRidge(tol=1e-6, fit_intercept=True, compute_score=True)
        if alpha is not None:
            reg.set_params(alpha_init=alpha)
        if lambda_ is not None:
            reg.set_params(lambda_init=lambda_)

        X_train, y_train, X_all = self.get_Xz(actions, target_column)

        reg.fit(X_train, y_train)
        ymean, ystd = reg.predict(X_all, return_std=True)
        return ymean, ystd, reg.scores_[-1]

    def get_Xz(self, actions, target_column):
        """get X and z

        Args:
            actions (np.array): a list of action
            target_column (str): a name of target columns in dataframe

        Returns:
            np.array: X training
            np.array: Y training
            np.array: X all
        """
        df_metadata = self.df_metadata
        normalized_feature_columns = df_metadata["normalized_feature_columns"]
        df = self.df
        X_train = df.loc[actions, normalized_feature_columns].values
        y_train = df.loc[actions, target_column].values
        X_all = df.loc[:, normalized_feature_columns].values
        return X_train, y_train, X_all

    def actions(self, actions_taken):
        """get next actions

        Args:
            actions_taken (np.array): a list of actions taken already

        Returns:
            np.array: action to take
        """
        zmean_list, zstd_list, score_list = [], [], []
        target_columns = self.df_metadata["target_columns"]

        for target_column in target_columns:
            X_train, y_train, X_all = self.get_Xz(actions_taken, target_column)

            if self.lambda_ is not None:
                zmean, zstd, score = self._fit_predict(
                    actions_taken, target_column, lambda_=self.lambda_)
            else:
                zmean, zstd, score = self._fit_predict(
                    actions_taken, target_column)

            zmean_list.append(zmean.tolist())
            zstd_list.append(zstd.tolist())
            score_list.append(score)

        self.score_list = score_list

        zmean, zstd = np.array(zmean_list).T, np.array(zstd_list).T

        zmean_columns = self.df_metadata["predicted_target_columns"]
        dfpredict = pd.DataFrame(zmean, columns=zmean_columns)

        zstd_columns = self.df_metadata["predicted_std_target_columns"]
        dfpredict_std = pd.DataFrame(zstd, columns=zstd_columns)

        scalizetarget = self.scalizetarget
        scalized_zmean, _ = scalizetarget.apply(zmean)
        scalized_zmean = scalized_zmean.reshape(-1)
        scalized_zstd = np.sqrt(np.sum(zstd**2, axis=1))
        _df = pd.DataFrame({"scalized_target": scalized_zmean,
                            "scalized_std_target": scalized_zstd})
        self.df = pd.concat([self.df, dfpredict, dfpredict_std, _df], axis=1)

        new_actions, acq_values = self._choose_action(
            scalized_zmean, scalized_zstd)

        del _df
        _df = pd.DataFrame({"acquisition": acq_values})
        self.df = pd.concat([self.df, _df], axis=1)

        return new_actions

    def _choose_action(self, y_test_p_mean, y_test_p_std):
        """choose next actions

        Args:
            y_test_p_mean (np.array): the mean value of y
            y_test_p_std (np.array): the stddev values of y

        Returns:
            np.array: a list of actions
            np.array: acquisition function values
        """
        method = self.method

        y_test_p_mean_transformed = y_test_p_mean

        if method['acquisition_function'] == 'TS':
            # Thompson sampling
            n = y_test_p_std.shape[0]
            if False:
                std_max = y_test_p_std.max()
                cov = np.random.normal(
                    scale=std_max*0.01, size=n*n).reshape(n, n)
                for i in range(n):
                    cov[i, i] = y_test_p_std[i]
            else:
                cov = np.diag(y_test_p_std)
            acq = multivariate_normal(
                mean=y_test_p_mean_transformed, cov=cov, allow_singular=True)
            acq_values = acq.rvs(size=1)
        elif method['acquisition_function'] == 'LCB':
            acq_values = y_test_p_mean_transformed - y_test_p_std

        index = np.argsort(acq_values)

        choose = method['optimize']
        if choose == "max":
            index = index[::-1]

        return index, acq_values


# In[101]:


def add_action(new_actions_list, actions, N_draw):
    """auxially function to select actions

    Args:
        new_actions_list (list): a list of next actions
        actions (np.array): actions taken already
        N_draw (int): a number of action to select for each new_actions_list

    Returns:
        [type]: [description]
    """
    new_actions_add_list = []
    for i, (new_actions) in enumerate(new_actions_list):
        new_actions_add = []
        for g_new_action in new_actions:
            if g_new_action not in actions and g_new_action not in new_actions_add_list:
                new_actions_add.append(g_new_action)
                print(i, "choose", g_new_action)
            else:
                # print(g_new_action, "is already chosen.")
                pass
            if len(new_actions_add) >= N_draw:
                new_actions_add_list.extend(new_actions_add)
                break

    print('add', new_actions_add_list)
    actions = np.hstack([actions, np.array(new_actions_add_list)])
    return actions


def try_One(df, df_metadata, actions, method_list, scalizedtarget,
            iteration, N_draw=1, show_img=False, figsize=g_figsize):

    print("actions", actions)

    size = df.shape[0]

    n_target = len(df_metadata["target_columns"])

    new_actions_list = []
    for method in method_list:
        #        for lambda_ in [1e-3, 1.]:
        for lambda_ in [1e-3]:

            opt = BayesianRidgeOpt(df, df_metadata, scalizetarget=scalizedtarget,
                                   method=method, lambda_=lambda_)
            new_actions = opt.actions(actions)

            if show_img:
                z_all = opt.df.loc[:, df_metadata["target_columns"]].values
                z_train = opt.df.loc[actions,
                                     df_metadata["target_columns"]].values
                zmean = opt.df.loc[:,
                                   df_metadata["predicted_target_columns"]].values
                zstd = opt.df.loc[:,
                                  df_metadata["predicted_std_target_columns"]].values
                score = opt.score_list

                fig, axes = plt.subplots(1, n_target+1, figsize=figsize)

                plot_train_test_fill(actions, z_all,
                                     z_train,
                                     zmean, zstd,
                                     "iteration = {},\nn = {}, \nmethod={}, lambda= {},\nscore={}".format(iteration,
                                                                                                          method, lambda_,
                                                                                                          size, score),
                                     axes)

                acq_values = opt.df.loc[:, "acquisition"].values
                scalized_zmean = opt.df.loc[:, "scalized_target"].values
                scalized_zstd = opt.df.loc[:, "scalized_std_target"].values

                plot_range(acq_values, scalized_zmean, scalized_zstd,
                           new_actions[:N_draw], axes[-1])

                fig.show()

            new_actions_list.append(new_actions)

    actions = add_action(new_actions_list, actions, N_draw)

    return actions


def show_target_space(actions, z_all, target_region, scalizedtarget,
                      N_first,
                      figsize=(5, 3)):

    if False:
        fig, ax = plt.subplots(figsize=g_figsize)

        ax.scatter(z_all[:, 0], z_all[:, 1])

        xy = [target_region[0][0], target_region[1][0]]
        width = target_region[0][1]-target_region[0][0]
        height = target_region[1][1]-target_region[1][0]
        rect = Rectangle(xy, width, height, alpha=0.2)
        ax.add_patch(rect)

        ax.legend()
        plt.show()

    for n in range(N_first, len(actions)+1, 2):
        fig, ax = plt.subplots(figsize=figsize)

        xy = [target_region[0][0], target_region[1][0]]
        width = target_region[0][1]-target_region[0][0]
        height = target_region[1][1]-target_region[1][0]
        rect = Rectangle(xy, width, height, alpha=0.2)
        ax.add_patch(rect)

        ax.scatter(z_all[:, 0], z_all[:, 1], c="lightgray")
        ax.set_title(str(n))
        i = actions[:n]
        ax.scatter(z_all[i, 0], z_all[i, 1], c="gray")
        a = i[-2:]
        ax.scatter(z_all[a, 0], z_all[a, 1], c="red")
        plt.show()

# In[109]:


if __name__ == "__main__":

    warnings.filterwarnings('ignore')

    N_first = 10
    g_N_draw = 1
    g_df, g_df_metadata = make_all_data(n_order=3)
    print(g_df.shape, g_df_metadata)

    g_actions = random.sample(
        range(g_df.shape[0]), N_first)  # the first ations
    g_actions = np.array(g_actions)
    print("actions", g_actions)

    # g_lambda_ = 1e-3
    g_lambda_ = 1.0

    g_figsize = (10, 3)

    g_iteration = 0

    g_method_list = []
    g_method_list.append({'acquisition_function': 'TS', 'optimize': 'min'})
    g_method_list.append({'acquisition_function': 'LCB', 'optimize': 'min'})

    print(g_method_list)

    for g_iteration in range(8):
        g_actions = try_One(g_df, g_df_metadata, g_actions,
                            g_method_list, g_scalizedtarget, g_iteration,
                            show_img=True)

    print(g_actions, len(g_actions))

    g_z_all = g_df.loc[:, g_df_metadata["target_columns"]].values
    show_target_space(g_actions, g_z_all, g_target_region,
                      g_scalizedtarget, N_first)

# %%
