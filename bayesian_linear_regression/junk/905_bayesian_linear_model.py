#!/usr/bin/env python
# coding: utf-8

# In[150]:


from multi_object_scalization import ScalizeTarget
import sys
import pandas as pd
import random
from scipy.stats import multivariate_normal
from sklearn.linear_model import BayesianRidge
import matplotlib.pyplot as plt
import numpy as np

print(__doc__)


sys.path.append(".")


functype = 2
if functype == 1:
    def func(x):
        return np.sin(2*np.pi*x)
    g_target_region = [[0.35, 0.45]]

elif functype == 2:
    def func(x):
        return np.sin(2*np.pi*x) + 0.3*np.cos(5*np.pi*x)**2
    g_target_region = [[0.5, 0.55]]


def plot_func(f):
    x = np.linspace(0., 1., 300)
    y = f(x)
    plt.plot(x, y)


plot_func(func)


# In[153]:


scalizetarget = ScalizeTarget(g_target_region)


# In[154]:


if False:
    def apply_func(y: np.array):
        """LeLU sum
        """
        return np.maximum(g_target_region[0]-y, 0) + np.maximum(y-g_target_region[1], 0)


# In[155]:


def test_apply_func():
    """try appying function to know what happens.
    """
    x = np.linspace(0., 1., 200)
    y = func(x)
    scalizetarget = ScalizeTarget(g_target_region)
    ya, _ = scalizetarget.apply(y)
    ya = ya.reshape(-1)
    plt.figure(figsize=(10, 3))
    plt.fill_between([0, 1], [g_target_region[0][0], g_target_region[0][0]],
                     [g_target_region[0][1], g_target_region[0][1]],
                     "--", color="green", alpha=0.2)
    plt.plot(x, y, color="red", label="y")
    plt.plot(x, ya, color="blue", label="scalized")

    plt.plot([0, 1], [0, 0], "--", color="blue")
    plt.ylabel('y')
    plt.legend()
    plt.show()

    plt.figure()
    plt.hist(y, bins=20, orientation='horizontal')
    plt.ylabel('scalized y')
    plt.xlabel('occurence')
    plt.show()


test_apply_func()


# In[156]:


g_rng = np.random.RandomState(0)
np.random.seed(0)


def make_all_data(n_order=3):
    """make all data
    """
    x_test = np.linspace(0., 1., 100)
    y_test = func(x_test)
    X_test = np.vander(x_test, n_order + 1, increasing=True)

    return X_test, x_test, y_test


# In[157]:


def plot_train_test(x_all, y_all,
                    x_train, y_train,
                    ypmean, ypstd,
                    title, ax):

    ax.fill_between(x_all, ypmean-ypstd,
                    ypmean+ypstd,
                    color="pink", alpha=0.5, label="$y_{predict std}$")
    ax.plot(x_all, ypmean, color="red", lw=2, label="$y_{predict mean}$")

    ax.fill_between([x_all.min(), x_all.max()],
                    [g_target_region[0][0], g_target_region[0][0]],
                    [g_target_region[0][1], g_target_region[0][1]],
                    "--", color="green", alpha=0.2)

    ax.plot(x_all, y_all, color="blue", lw=2, label="$y_{expr}$")

    ax.scatter(x_train, y_train, s=50, alpha=1, label="$y_{train}$")
    ax.set_title(title)
    ax.set_ylabel('y')
    ax.legend()


# In[160]:


def plot_range(y_p_mean, g_y_std, index, title=None, ax=None):

    x = range(y_p_mean.shape[0])
    ax.fill_between(x, y_p_mean.ravel()-g_y_std,
                    y_p_mean.ravel()+g_y_std,
                    color="pink", alpha=0.5, label="$y_{predict std}$")
    ax.plot(x, y_p_mean.ravel(), color='blue')
    ax.scatter(index, y_p_mean.ravel()[index],
               color='red', label="new selection")
    ax.set_ylabel('acq')
    ax.legend()
    if title is not None:
        ax.set_title(title)


# In[161]:


class BayesianRidgeOpt:
    """Bayesian optimiation using Bayesian Ridge regression
    """

    def __init__(self, method=None):
        """initialization

        Args:
            method (dict, optional): parameters. Defaults to None.
        """
        _method = {'acquisition_function': 'TS', 'optimize': 'min'}
        if method is None:
            self.method = _method
        else:
            _method.update(method)
            self.method = _method

    def predict(self, X_train, y_train, X_test, alpha=None, lambda_=None):
        """fit and predict

        Args:
            X_train (np.array): X
            y_train (np.array): y
            X_test (np.array): X to predict
            alpha (float, optional): alpha of BayesianRidgeRegression. Defaults to None.
            lambda_ (float, optional): lambda_ of BayesianRidgeRegression. Defaults to None.

        Returns:
            np.array: the mean values of prediction y
            np.array: the stddev values of prediction y
            float: the last likelihood
        """
        reg = BayesianRidge(tol=1e-6, fit_intercept=False, compute_score=True)
        if alpha is not None:
            reg.set_params(alpha_init=alpha)
        if lambda_ is not None:
            reg.set_params(lambda_init=lambda_)
        reg.fit(X_train, y_train)
        ymean, ystd = reg.predict(X_test, return_std=True)
        return ymean, ystd, reg.scores_[-1]

    def choose_action(self, y_test_p_mean, y_test_p_std, N=2):
        """choose next actions

        Args:
            y_test_p_mean (np.array): the mean value of y
            y_test_p_std (np.array): the stddev values of y
            N (int, optional): the number of actions to draw. Defaults to 2.

        Returns:
            np.array: a list of actions
            np.array: acquisition function values
        """
        method = self.method

        y_test_p_mean_transformed = y_test_p_mean

        if method['acquisition_function'] == 'TS':
            # Thompson sampling
            n = y_test_p_std.shape[0]
            if False:
                std_max = y_test_p_std.max()
                cov = np.random.normal(
                    scale=std_max*0.01, size=n*n).reshape(n, n)
                for i in range(n):
                    cov[i, i] = y_test_p_std[i]
            else:
                cov = np.diag(y_test_p_std)
            acq = multivariate_normal(
                mean=y_test_p_mean_transformed, cov=cov, allow_singular=True)
            acq_values = acq.rvs(size=1)
        elif method['acquisition_function'] == 'LCB':
            acq_values = y_test_p_mean_transformed - y_test_p_std

        index = np.argsort(acq_values)

        choose = method['optimize']
        if choose == "max":
            index = index[::-1]

        return index, acq_values


# In[167]:


g_N_draw = 1
g_X_all, g_x_all, g_y_all = make_all_data(n_order=3)

g_actions = random.sample(range(g_x_all.shape[0]), 5)  # the first ations
g_actions = np.array(g_actions)
print("actions", g_actions)

g_lambda_ = 1e-3
# lambda_ = 1.0

g_figsize = (10, 3)

g_iteration = 0

g_method_list = []
g_method_list.append({'acquisition_function': 'TS', 'optimize': 'min'})
g_method_list.append({'acquisition_function': 'LCB', 'optimize': 'min'})

print(g_method_list)

while True:

    g_iteration += 1
    g_X_train = g_X_all[g_actions]
    g_x_train = g_x_all[g_actions]
    g_y_train = g_y_all[g_actions]

    figure, ax = plt.subplots(1, 1, figsize=(5, 3))
    ax.hist(g_y_all, bins=20, orientation='horizontal')
    ax.hist(g_y_train, bins=20, orientation='horizontal', label='chosen')
    xlim = ax.get_xlim()
    ax.set_xlim(xlim)
    ax.legend()
    ax.fill_between(xlim, [g_target_region[0][0], g_target_region[0][0]],
                    [g_target_region[0][1], g_target_region[0][1]],
                    color='green', alpha=0.1)
    ax.set_ylabel('y')
    ax.set_xlabel('occurence')
    plt.show()

    g_size = g_X_train.shape[0]
    if g_size > 30:
        break

    g_new_actions_list = []
    g_score_list = []
    for g_method in g_method_list:
        for g_lambda_ in [1e-3, 1.]:

            print(g_method, g_lambda_)

            opt = BayesianRidgeOpt(method=g_method)

            g_ymean, g_ystd, g_score = opt.predict(
                g_X_train, g_y_train, g_X_all, lambda_=g_lambda_)

            scaliedtarget = ScalizeTarget(g_target_region)
            g_scalized_ymean, _ = scaliedtarget.apply(g_ymean)
            g_scalized_ymean = g_scalized_ymean.reshape(-1)

            g_new_actions, g_acq_values = opt.choose_action(
                g_scalized_ymean, g_ystd, 1)

            fig, ax = plt.subplots(1, 2, figsize=g_figsize)
            plot_train_test(g_x_all, g_y_all,
                            g_x_train, g_y_train,
                            g_scalized_ymean, g_ystd,
                            "iteration = {}, n = {}, score={:.2f}".format(g_iteration,
                                                                          g_size, g_score),
                            ax=ax[0])
            plot_range(g_acq_values, g_ystd,
                       g_new_actions[:g_N_draw], ax=ax[1])
            fig.tight_layout()
            fig.show()

            g_new_actions_list.append(g_new_actions)
            g_score_list.append(g_score)

    print("plan", g_new_actions[:10], "...")
    # pick up the first N points
    g_new_actions_add_list = []
    for i, (g_new_actions, g_score) in enumerate(zip(g_new_actions_list, g_score_list)):
        g_new_actions_add = []
        for g_new_action in g_new_actions:
            if g_new_action not in g_actions:
                g_new_actions_add.append(g_new_action)
                print(i, "score", g_score, "choose", g_new_action)
            else:
                print(g_new_action, "is done chosen.")
            if len(g_new_actions_add) >= g_N_draw:
                g_new_actions_add_list.extend(g_new_actions_add)
                break

    print('add', g_new_actions_add_list)
    g_actions = np.hstack([g_actions, np.array(g_new_actions_add_list)])


count = 0
for n in range(5, g_actions.shape[0]+1, 2):
    count += 1

fig, ax_all = plt.subplots(1, count, figsize=(20, 5))
bins = 20

for i, n in enumerate(range(5, g_actions.shape[0]+1, 2)):

    ax = ax_all[i]
    actions = g_actions[:n]

    ax.hist(g_y_all, bins=bins, orientation="horizontal", alpha=0.5)
    ax.hist(g_y_all[actions], range=(g_y_all.min(), g_y_all.max()),
            bins=bins, orientation="horizontal")
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    ax.fill_between([xlim[0], xlim[1]], [g_target_region[0][0], g_target_region[0][0]],
                    [g_target_region[0][1], g_target_region[0][1]], color='green', alpha=0.1)
    ax.axis('off')
    # ax.tick_params(axis='x',labelbottom='off')
fig.tight_layout()
fig.show()


# In[ ]:
