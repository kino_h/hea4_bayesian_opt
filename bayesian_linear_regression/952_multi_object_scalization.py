# %%

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


class ScalizeTarget:
    """make scalized target variable made of multi dimnesional target variables
    """

    def __init__(self, target_region: (np.array, list), scale: (np.array, None),
            func_type="sqrt_sum_ReLU"):
        """initialization

        Args:
            func (function): function to apply
        """
        self.func_type = func_type
        if func_type == "sqrt_sum_ReLU":
            self.func = self.sqrt_sum_LeLU
        else:
            raise ValueError("unknown func_type {}".format(func))
        self.target_region = np.array(target_region)

        n = self.target_region.shape[0]
        print("target dimension", n)

        if scale is not None:
            self.scale = scale
        else:
            self.scale = np.eye((n))
        print("self.scale", scale)

    def info(self):
        return {"target_region":self.target_region, "scale:":self.scale}

    def apply_df(self, df, columns):
        """apply function to df[columns]

        Args:
            df (pd.DataFrame): data
            columns (list): a list of columns of data

        Returns:
            pd.DataFrame: with scalized target variable can be accessed by "scalied_target" column
        """
        func = self.func
        x = df.loc[:, columns].values
        raise ValueError("apply_df, std is necessary")
        acq, v, v_std = func(x)

        new_columns = []
        for col in columns:
            new_columns.append("applied_{}".format(col))
        df_acq = pd.DataFrame(v, columns=new_columns)
        df_all = pd.concat([df, df_acq], axis=1)
        df_all["scalied_target"] = acq

        return df_all

    def apply(self, x: np.array, std: np.array=None):
        """apply function to df[columns]

        Args:
            x (np.array): data

        Returns:
            np.array: scalized target variable
            np.array: tranformed target variable
        """
        func = self.func
        if len(x.shape) == 1:
            x = x.reshape(-1, 1)
        if std is None:
            acq, v = func(x, std)
            return acq, v
        else:
            acq, v, v_std = func(x, std)
            return acq, v, v_std

    def sqrt_sum_LeLU(self, xy: np.array, std:np.array=None, sum_type="rse"):
        """LeLU sum and apply sum_type

        Args:
            xy (np.array): data
            std (np.array): stddev of data
            sum_type (str, optional): scalized funtion ["rse, or "ae"]. Defaults to "rse".

        Returns:
            [type]: [description]
        """
        target_region = self.target_region
        v_list = []
        for i in range(xy.shape[1]):
            v = np.maximum(target_region[i, 0]-xy[:, i], 0) \
                + np.maximum(xy[:, i]-target_region[i, 1], 0)
            v_list.append(v.tolist())
        v_ = np.array(v_list).T
        # v_ can be negative. v - v.min() is necessary!
        v_ = v_ - v_.min(axis=0)

        mul = 1./np.array(self.scale)
        print("target variable scale",mul)
        v = (v_*mul)
        if std is not None:
            std_v = (std*mul)

        if sum_type == 'rse':
            acq = np.sqrt(np.sum(v**2, axis=1)).reshape(-1)
        elif sum_type == 'ae':
            acq = np.sum(v, axis=1).reshape(-1)
        if std is not None:
            return acq, v, std_v
        else:
            return acq, v


def make_targt_df():
    """make target dataframe

    Returns:
        pd.DataFrame: data
        tuple: data shape to show in 2D
    """
    x_list = []
    y_list = []
    for x in np.arange(-5, 6):
        x_list.append(x)
    for y in np.arange(-6, 7):
        y_list.append(y)

    X, Y = np.meshgrid(x_list, y_list)

    display_shape = X.shape

    df_target = pd.DataFrame({"target_0": X.ravel(), "target_1": Y.ravel()})

    return df_target, display_shape


def plot_surf(df_all, display_shape):
    """plot surface

    Args:
        df_all (pd.DataFrame): data
        display_shape (tuple): data shape to show it in 3D
    """
    X = df_all["target_0"].values.reshape(display_shape)
    Y = df_all["target_1"].values.reshape(display_shape)
    Z = df_all["scalied_target"].values.reshape(display_shape)

    fig = plt.figure(figsize=(9, 9), facecolor="w")
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_surface(X, Y, Z, alpha=0.5,   linewidth=0, antialiased=False)
    ax.contour(X, Y, Z,  offset=-1, colors="k", linestyles="solid")
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    plt.show()


def plot_surf_target(df_all, target, display_shape):
    """plot surface

    Args:
        df_all (pd.DataFrame): data       
        target (str): target columns in data
        display_shape (tuple): data shape to show in 3D
    """
    Z = df_all[target].values.reshape(display_shape)
    x_list = np.arange(display_shape[0])
    y_list = np.arange(display_shape[1])
    print(x_list.shape, y_list.shape)
    X, Y = np.meshgrid(y_list, x_list)

    print(X.shape, Y.shape, Z.shape)

    fig = plt.figure(figsize=(9, 9), facecolor="w")
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_surface(X, Y, Z, alpha=0.5,   linewidth=0, antialiased=False)
    ax.contour(X, Y, Z,  offset=-1, colors="k", linestyles="solid")
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    plt.show()


# %%
if __name__ == "__main__":
    g_df_target, g_display_shape = make_targt_df()
    g_df_all = ScalizeTarget([[1, 2], [-2, 0]]).apply_df(
        g_df_target, ["target_0", "target_1"])

    plot_surf(g_df_all, g_display_shape)
    # plot_surf_target(g_df_all, "target_1", g_display_shape)

    print("all done")

# %%
