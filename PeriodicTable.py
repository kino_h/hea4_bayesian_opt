from pymatgen.core.periodic_table import Element
import numpy as np
import pandas as pd
import json


class PeriodicTable:
    """Periodic Table supplementry class

    zmax : max number of z +1
    symbol_list : atomic element names from H,...
    row_list: atomic element row from H,...
    group_list: atomic element group from H,...
    row_max: max. number of row_list
    group_max: max. number of group_list
    """

    def __init__(self):
        """initialization
        """
        # self.make_config()
        # self.make_features()
        self.make_zlist()
        self.make_valence_config()
        self.make_orbital_vector()
        self.make_group_row_existence()

        self.df.set_index("z", inplace=True)

    def make_zlist(self):
        """make fundamental data of Elements
        """
        self.zmax = 104
        self.z_list = list(range(1, self.zmax))
        symbol_list = []
        row_list = []
        group_list = []
        for z in self.z_list:
            elm = Element('H').from_Z(z)
            symbol_list.append(str(elm))
            row_list.append(elm.row)
            group_list.append(elm.group)

        self.group_max = np.max(group_list)
        self.row_max = np.max(row_list)

        df = pd.DataFrame(
            {'z': self.z_list, 'symbol': symbol_list, 'row': row_list, 'group': group_list})
        self.df = df

    def make_valence_config(self):
        """make valence configuration (internal data)
        """
        valence_config_list = []
        for z in self.df["z"].values:
            elm = Element('H').from_Z(z)
            electronic_structure = elm.electronic_structure.split(".")
            if electronic_structure[0].startswith("["):
                electronic_structure = electronic_structure[1:]
            valence_config = []
            for e_struc in electronic_structure:
                n = int(e_struc[:1])
                if len(e_struc[1:]) > 5:
                    print("configuration warning:", elm, e_struc)
                    s = e_struc[1:]
                    # if exists
                    s = s.replace(' (tentative)', '')
                    valence_config.append([n, s])
                else:
                    s = e_struc[1:]
                    valence_config.append([n, s])
            valence_config_list.append(json.dumps(valence_config))
        self.df["valence_config"] = valence_config_list

    def make_e_config_template(self):
        """make electron configuration template
        """
        e_config_list = []
        e_config_list.extend(['s1', 's2'])
        for n in range(1, 7):
            e_config_list.append("p{}".format(n))
        for n in range(1, 11):
            e_config_list.append("d{}".format(n))
        for n in range(1, 15):
            e_config_list.append("f{}".format(n))
        e_config_dic = {}
        for i, config in enumerate(e_config_list):
            e_config_dic[config] = i
        self.e_config_dic = e_config_dic
        self.e_config_list = np.array(e_config_list)

    def make_orbital_vector(self):
        """make orbital field vector
        """
        self.make_e_config_template()

        e_config_list = self.e_config_list
        e_config_dic = self.e_config_dic

        valence_vector_list = []

        for e_config in self.df["valence_config"].values:
            # json to list
            e_config = json.loads(e_config)
            e_config = np.array(e_config)

            valence_vector = np.zeros((e_config_list.shape[0]))
            vtrue = []
            for config in e_config:
                vtrue.append(e_config_dic[config[1]])
            vtrue = np.array(vtrue)
            valence_vector[vtrue] = 1

            valence_vector_list.append(list(valence_vector))

        df_valence = pd.DataFrame(valence_vector_list, columns=e_config_list)

        self.df = pd.concat([self.df, df_valence], axis=1)

    def make_group_row_existence(self):
        """make group row existence
        coresponding to group, row version of orbital field vector
        """
        group_vector = np.zeros((self.df.shape[0], self.group_max+1))
        row_vector = np.zeros((self.df.shape[0], self.row_max+1))
        for i, (group, row) in enumerate(zip(self.df["group"], self.df["row"])):

            group_vector[i, group] = 1.0
            row_vector[i, row] = 1.0

        group_columns = ["group{}".format(i) for i in range(self.group_max+1)]
        df_group = pd.DataFrame(group_vector, columns=group_columns)
        del df_group["group0"]
        row_columns = ["row{}".format(i) for i in range(self.row_max+1)]
        df_row = pd.DataFrame(row_vector, columns=row_columns)
        del df_row["row0"]

        self.df = pd.concat([self.df, df_group, df_row], axis=1)
