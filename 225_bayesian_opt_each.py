#!/usr/bin/env python
# coding: utf-8

import os
import sys

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import pandas as pd

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.model_selection import KFold, train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
import time
import numpy as np
import warnings
import random
import json


sys.path.append('/home/kino/work/00_HEA/python_module')
from hea4f3 import Hea4Feature3data
sys.path.append('.')

from Common import load_save_pickle
from Common import make_features
from Common import do_bayselinear_regresson_X2


sys.path.append("bayesian_linear_regression_test")
from multi_object_scalization import ScalizeTarget
from BayesianRidgeOpt import BayesianRidgeOpt, add_action, try_One






def plot_targetspace(df, metadata, target_region, figsize=(5,5)):
    """plot targets

    Args:
        df (pd.DataFrame): data
        metadata (dict): metadata
        target_region (list): target region
    """
    target_columns, sc_target_column = metadata["target_columns"], "scalized_target"

    zall = df.loc[:, target_columns].values
    ta = df.loc[:, sc_target_column].values

    fig, ax = plt.subplots(figsize=figsize)

    xy = [target_region[0][0], target_region[1][0]]
    width = target_region[0][1]-target_region[0][0]
    height = target_region[1][1]-target_region[1][0]
    rect = Rectangle(xy, width, height, alpha=0.2)
    ax.add_patch(rect)

    img = ax.scatter(zall[:, 0], zall[:, 1], c=ta)
    plt.colorbar(img)
    i = ta == 0
    ax.scatter(zall[i, 0], zall[i, 1], c="red")
    ax.set_xlabel(target_columns[0])
    ax.set_ylabel(target_columns[1])
    ax.legend()
    fig.tight_layout()
    fig.show()








def df_feature_normalize(df, metadata):
    feature_columns = metadata["feature_columns"]
    normalized_feature_columns = []
    for feature in feature_columns:
        normalized_feature_columns.append("normalized_{}".format(feature))
    scaler = StandardScaler()
    normalized_x = scaler.fit_transform(df.loc[:, feature_columns].values)
    dfnormalizedfeature = pd.DataFrame(
        normalized_x, columns=normalized_feature_columns)
    metadata.update({"normalized_feature_columns": normalized_feature_columns})
    
    if normalized_feature_columns[0] in list(df.columns):
        print("exist")
        df.drop(columns=normalized_feature_columns, inplace=True)
    df = pd.concat([df, dfnormalizedfeature], axis=1)
    
    predicted_target_columns = []
    tr_predicted_target_columns = []
    predicted_std_target_columns = []
    acqusition_target_columns = []
    target_columns = metadata["target_columns"]
    for target in target_columns:
        predicted_target_columns.append("predicted_{}".format(target))
        predicted_std_target_columns.append("predicted_std_{}".format(target))
        tr_predicted_target_columns.append("transformed_predicted_{}".format(target))
        acqusition_target_columns.append("acquisition_{}".format(target))
    metadata.update({"acquisition_target_columns": acqusition_target_columns})
    metadata.update({"predicted_target_columns": predicted_target_columns})
    metadata.update({"transformed_predicted_target_columns": tr_predicted_target_columns})
    metadata.update(
        {"predicted_std_target_columns": predicted_std_target_columns})

    return df, metadata




# get_ipython().run_line_magic('matplotlib', 'inline')

def all_regression(df_feature_target, features, target_list):
    """execute all the regression and make resulting dict

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_list (str): a name of target variable

    Returns:
        dict: R2 results for all the regressions.
    """    
    result_dic_all = {}
    for target_name in  target_list:
        result_dic_all[target_name] = {}

    for target_name in  target_list:
        print(target_name)

        result = do_bayselinear_regresson_X2(df_feature_target, features, target_name)
        result_dic_all[target_name].update(result)            

    return result_dic_all





def show_target_space(actions, df, metadata, scalizedtarget,
                      N_draw, iteration, 
                      figsize=(5, 5)):

    n = len(actions)
    fig, ax = plt.subplots(figsize=figsize)
    target_region = scalizedtarget.target_region
    xy = [target_region[0][0], target_region[1][0]]
    width = target_region[0][1]-target_region[0][0]
    height = target_region[1][1]-target_region[1][0]
    rect = Rectangle(xy, width, height, alpha=0.2)
    ax.add_patch(rect)

    z_all = df.loc[:, metadata["target_columns"]].values
    ax.set_xlabel(metadata["target_columns"][0])
    ax.set_ylabel(metadata["target_columns"][1])

    ax.scatter(z_all[:, 0], z_all[:, 1], c="lightgray")
    ax.set_title(str(iteration))
    i = actions
    ax.scatter(z_all[i, 0], z_all[i, 1], c="gray")
    a = i[-N_draw:]
    ax.scatter(z_all[a, 0], z_all[a, 1], c="red")
    plt.tight_layout()

    prefix = "iteration_image"
    if not os.path.exists(prefix):
        os.mkdir(prefix)
    plt.savefig(os.path.join(prefix,"A{:08d}".format(iteration)))
    plt.show()


def setup_data(scalizedtarget):

    target_region = scalizedtarget.target_region
    scale = scalizedtarget.scale

    calctype="asa"
    polytype="fcc"

    hea4 = Hea4Feature3data(calctype)

    status, df_calc_polytype_loaded = load_save_pickle("df_extend_{}_{}.pickle".format(calctype, polytype),
                                                    None, action=None)
    print(status)
    if status == "loaded":
        df_calc_polytype = df_calc_polytype_loaded
        print("shape",df_calc_polytype.shape)


    df_feature_target = df_calc_polytype.query(hea4.querystr_by_local_moment(add_not=True))
    df_feature_target.reset_index(drop=True, inplace=True)
    print("data shape", df_feature_target.shape)

    df_feature_target["TC"] = df_feature_target["TC(K)"]
    df_feature_target['resistivity'] = df_feature_target['resistivity(micro ohm cm)']

    features = make_features(["phys_prop_mean_std", "basic_mean_std"])

    target_list = ['TC', 'resistivity']
    # target_list = ['resistivity']

    metadata = {"target_columns": target_list, "feature_columns": features}

    if len(target_list)>1:
        df_feature_target.plot(target_list[0], target_list[1], kind="scatter")

    zall = df_feature_target.loc[:, target_list].values
    ta, v = scalizedtarget.apply(zall)
    sc_target_column = "scalized_target"
    dfsctarget = pd.DataFrame(ta, columns=[sc_target_column])
    if sc_target_column in list(df_feature_target.columns):
        del df_feature_target[sc_target_column]
    df_feature_target = pd.concat([df_feature_target, dfsctarget], axis=1)


    plot_targetspace(df_feature_target, metadata, target_region)

    df_feature_target, metadata = df_feature_normalize(df_feature_target, metadata)
    for key in metadata.keys():
        print(key, metadata[key])

    result_dic_all = all_regression(df_feature_target, metadata["normalized_feature_columns"], target_list)
    print(result_dic_all)

    return df_feature_target, metadata

def setup_data_simple():
    prefix = "bayesian_linear_regression_test"
    df = pd.read_csv(os.path.join(prefix,"simple.csv"))
    with open(os.path.join(prefix,"simple_metadata.json")) as f:
        metadata = json.load(f)
    return df, metadata

if __name__ == "__main__":
    
    random.seed(3)

    warnings.filterwarnings('ignore')
    functype =  1
    if functype == 1:
        g_target_region = [[800, 1200],[0,25]]
        g_scale = [200000,10]
        g_scalizedtarget = ScalizeTarget(g_target_region, g_scale)
        g_df_feature_target, g_metadata = setup_data(g_scalizedtarget)
        N_first = 50
        g_N_draw = 5
        # g_target_list
        print(*list(g_df_feature_target.columns))
        g_target_list = g_metadata["target_columns"]
        querystr = "{}<{} and {}<{}".format(g_target_list[0],200, 150, g_target_list[1])
        print(querystr)
        g_df_init = g_df_feature_target.query(querystr)
        index = list(g_df_init.index)
        g_actions = random.sample(index, N_first)
        print("actions", len(g_actions), g_actions)

    elif functype == 2:
        g_target_region = [[0, 0.5], [0.7, 1.2]]
        g_scale = [0.1, 0.3]
        g_scalizedtarget = ScalizeTarget(g_target_region, g_scale)
        g_df_feature_target, g_metadata = setup_data_simple()
        N_first = 5
        g_N_draw = 1
        # g_target_list
        print(*list(g_df_feature_target.columns))
        g_target_list = g_metadata["target_columns"]
        if False:
            querystr = "{}<{}".format(g_target_list[1],-0.5)
            print(querystr)
            g_df_init = g_df_feature_target.query(querystr)
            index = list(g_df_init.index)
        else:
            index = list(g_df_feature_target.index)
        g_actions = random.sample(index, N_first)
        print("actions", len(g_actions), g_actions)
 

    # g_lambda_ = 1e-3
    g_lambda_ = 1.0

    g_figsize = (10, 3)

    g_iteration = 0

    g_method_list = []
    g_method_list.append({'acquisition_function': 'TS', 'optimize': 'min'})
    # g_method_list.append({'acquisition_function': 'LCB', 'optimize': 'min'})
    print(g_method_list)

    show_target_space(g_actions, g_df_feature_target, g_metadata,
            g_scalizedtarget, N_first, 0)

    g_iteration_max = 100
    for g_iteration in range(1,g_iteration_max):
        print("(r2) iteration",g_iteration,"of", g_iteration_max,
                len(g_actions), float(len(g_actions)/g_df_feature_target.shape[0]))
        g_actions = try_One(g_df_feature_target, g_metadata, g_actions,
                            g_method_list, g_scalizedtarget, N_draw=g_N_draw)

        # plot_mesh(g_df_feature_target, g_metadata, g_scalizedtarget, g_iteration)

        show_target_space(g_actions, g_df_feature_target, g_metadata,
                g_scalizedtarget, g_N_draw, g_iteration)
