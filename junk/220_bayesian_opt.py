#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import sys

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

sys.path.append('/home/kino/work/00_HEA/python_module')
from hea4f3 import Hea4Feature3data

g_calctype="asa"
g_polytype="fcc"

g_hea4 = Hea4Feature3data(g_calctype)


# In[2]:


sys.path.append('.')

from Common import load_save_pickle


# In[3]:


status, g_df_calc_polytype_loaded = load_save_pickle("df_extend_{}_{}.pickle".format(g_calctype, g_polytype),
                                                None, action=None)
print(status)
if status == "loaded":
    g_df_calc_polytype = g_df_calc_polytype_loaded
    print("shape",g_df_calc_polytype.shape)


# In[4]:


g_df_feature_target = g_df_calc_polytype.query(g_hea4.querystr_by_local_moment(add_not=True))
g_df_feature_target.reset_index(drop=True, inplace=True)
print("data shape", g_df_feature_target.shape)

g_df_feature_target["TC"] = g_df_feature_target["TC(K)"]
g_df_feature_target['resistivity'] = g_df_feature_target['resistivity(micro ohm cm)']


from Common import make_features

g_features = make_features(["phys_prop_mean_std", "basic_mean_std"])

g_target_list = ['TC', 'resistivity']

g_df_metadata = {"target_columns": g_target_list, "feature_columns": g_features}


g_df_feature_target.plot(g_target_list[0], g_target_list[1], kind="scatter")


# In[9]:


import pandas as pd

sys.path.append("bayesian_linear_regression_test")
from multi_object_scalization import ScalizeTarget


g_target_region = [[800, 1200],[125,175]]
g_scale = [20,25]
g_scalizedtarget = ScalizeTarget(g_target_region, g_scale)
zall = g_df_feature_target.loc[:, g_target_list].values
ta, v = g_scalizedtarget.apply(zall)
sc_target_column = "scalized_target"
dfsctarget = pd.DataFrame(ta, columns=[sc_target_column])
if sc_target_column in list(g_df_feature_target.columns):
    del g_df_feature_target[sc_target_column]
g_df_feature_target = pd.concat([g_df_feature_target, dfsctarget], axis=1)


# In[10]:



from matplotlib.patches import Rectangle

def plot_targetspace(df, df_metadata, target_region, figsize=(5,5)):
    """plot targets

    Args:
        df (pd.DataFrame): data
        df_metadata (dict): metadata
        target_region (list): target region
    """
    target_columns, sc_target_column = df_metadata["target_columns"], "scalized_target"

    zall = df.loc[:, target_columns].values
    ta = df.loc[:, sc_target_column].values

    fig, ax = plt.subplots(figsize=figsize)

    xy = [target_region[0][0], target_region[1][0]]
    width = target_region[0][1]-target_region[0][0]
    height = target_region[1][1]-target_region[1][0]
    rect = Rectangle(xy, width, height, alpha=0.2)
    ax.add_patch(rect)

    img = ax.scatter(zall[:, 0], zall[:, 1], c=ta)
    plt.colorbar(img)
    i = ta == 0
    ax.scatter(zall[i, 0], zall[i, 1], c="red")
    ax.set_xlabel(target_columns[0])
    ax.set_ylabel(target_columns[1])
    ax.legend()
    fig.tight_layout()
    fig.show()


# In[12]:


plot_targetspace(g_df_feature_target, g_df_metadata, g_target_region)


# In[14]:


from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import r2_score
from sklearn.model_selection import KFold, train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
import time
import numpy as np


# In[15]:


def df_feature_normalize(df, df_metadata):
    feature_columns = df_metadata["feature_columns"]
    normalized_feature_columns = []
    for feature in feature_columns:
        normalized_feature_columns.append("normalized_{}".format(feature))
    scaler = StandardScaler()
    normalized_x = scaler.fit_transform(df.loc[:, feature_columns].values)
    dfnormalizedfeature = pd.DataFrame(
        normalized_x, columns=normalized_feature_columns)
    df_metadata.update({"normalized_feature_columns": normalized_feature_columns})
    
    if normalized_feature_columns[0] in list(df.columns):
        print("exist")
        df.drop(columns=normalized_feature_columns, inplace=True)
    df = pd.concat([df, dfnormalizedfeature], axis=1)
    
    predicted_target_columns = []
    predicted_std_target_columns = []
    target_columns = df_metadata["target_columns"]
    for target in target_columns:
        predicted_target_columns.append("predicted_{}".format(target))
        predicted_std_target_columns.append("predicted_std_{}".format(target))
    df_metadata.update({"predicted_target_columns": predicted_target_columns})
    df_metadata.update(
        {"predicted_std_target_columns": predicted_std_target_columns})

    return df, df_metadata

g_df_feature_target, g_df_metadata = df_feature_normalize(g_df_feature_target, g_df_metadata)
for key in g_df_metadata.keys():
    print(key, g_df_metadata[key])


# In[16]:


import random
random.seed(1)


# In[17]:


from Common import do_bayselinear_regresson_X2


# In[18]:


# get_ipython().run_line_magic('matplotlib', 'inline')

def all_regression(df_feature_target, features, target_list):
    """execute all the regression and make resulting dict

    Args:
        df_feature_target (DataFrame): data
        features (list): a list of feature names
        target_list (str): a name of target variable

    Returns:
        dict: R2 results for all the regressions.
    """    
    result_dic_all = {}
    for target_name in  target_list:
        result_dic_all[target_name] = {}

    for target_name in  target_list:
        print(target_name)

        result = do_bayselinear_regresson_X2(df_feature_target, features, target_name)
        result_dic_all[target_name].update(result)            

    return result_dic_all

g_result_dic_all = all_regression(g_df_feature_target, g_df_metadata["normalized_feature_columns"], g_target_list)




print(g_result_dic_all)




from BayesianRidgeOpt import BayesianRidgeOpt, add_action, try_One


def show_target_space(actions, z_all, target_region, scalizedtarget,
                      N_draw, iteration, 
                      figsize=(5, 3)):

    n = len(actions)
    fig, ax = plt.subplots(figsize=figsize)

    xy = [target_region[0][0], target_region[1][0]]
    width = target_region[0][1]-target_region[0][0]
    height = target_region[1][1]-target_region[1][0]
    rect = Rectangle(xy, width, height, alpha=0.2)
    ax.add_patch(rect)

    ax.scatter(z_all[:, 0], z_all[:, 1], c="lightgray")
    ax.set_title(str(iteration))
    i = actions
    ax.scatter(z_all[i, 0], z_all[i, 1], c="gray")
    a = i[-N_draw:]
    ax.scatter(z_all[a, 0], z_all[a, 1], c="red")

    prefix = "iteration_image"
    if not os.path.exists(prefix):
        os.mkdir(prefix)
    plt.savefig(os.path.join(prefix,"{:08d}".format(iteration)))
    plt.show()


import warnings

if __name__ == "__main__":
    
    warnings.filterwarnings('ignore')

    N_first = 100
    g_N_draw = 5

    # g_target_list
    print(*list(g_df_feature_target.columns))
    querystr = "{}<{} and {}<{}".format(g_target_list[0],200, g_target_list[1],50)
    print(querystr)
    g_df_init = g_df_feature_target.query(querystr)
    index = list(g_df_init.index)
    g_actions = random.sample(index, N_first)
    print("actions", len(g_actions), g_actions)

    # g_lambda_ = 1e-3
    g_lambda_ = 1.0

    g_figsize = (10, 3)

    g_iteration = 0

    g_method_list = []
    g_method_list.append({'acquisition_function': 'TS', 'optimize': 'min'})
    # g_method_list.append({'acquisition_function': 'LCB', 'optimize': 'min'})
    print(g_method_list)

    g_z_all = g_df_feature_target.loc[:, g_df_metadata["target_columns"]].values
    show_target_space(g_actions, g_z_all, g_target_region,
                      g_scalizedtarget, N_first, 0)

    g_iteration_max = 1000
    for g_iteration in range(1,g_iteration_max):
        print("(r2) iteration",g_iteration,"of", g_iteration_max,
                len(g_actions), float(len(g_actions)/g_df_feature_target.shape[0]))
        g_actions = try_One(g_df_feature_target, g_df_metadata, g_actions,
                            g_method_list, g_scalizedtarget, N_draw=g_N_draw)


        show_target_space(g_actions, g_z_all, g_target_region,
                          g_scalizedtarget, g_N_draw*len(g_method_list), g_iteration)

