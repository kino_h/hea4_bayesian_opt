def setup_data():
    calctype="asa"
    polytype="fcc"

    hea4 = Hea4Feature3data(calctype)

    status, df_calc_polytype_loaded = load_save_pickle("df_extend_{}_{}.pickle".format(calctype, polytype),
                                                    None, action=None)
    print(status)
    if status == "loaded":
        df_calc_polytype = df_calc_polytype_loaded
        print("shape",df_calc_polytype.shape)


    df_feature_target = df_calc_polytype.query(hea4.querystr_by_local_moment(add_not=True))
    df_feature_target.reset_index(drop=True, inplace=True)
    print("data shape", df_feature_target.shape)

    df_feature_target["TC"] = df_feature_target["TC(K)"]
    df_feature_target['resistivity'] = df_feature_target['resistivity(micro ohm cm)']

    features = make_features(["phys_prop_mean_std", "basic_mean_std"])

    target_list = ['TC', 'resistivity']
    # target_list = ['resistivity']

    df_metadata = {"target_columns": target_list, "feature_columns": features}

    if len(target_list)>1:
        df_feature_target.plot(target_list[0], target_list[1], kind="scatter")

    target_region = [[800, 1200],[25,50]]
    scale = [20,10]
    scalizedtarget = ScalizeTarget(target_region, scale)
    zall = df_feature_target.loc[:, target_list].values
    ta, v = scalizedtarget.apply(zall)
    sc_target_column = "scalized_target"
    dfsctarget = pd.DataFrame(ta, columns=[sc_target_column])
    if sc_target_column in list(df_feature_target.columns):
        del df_feature_target[sc_target_column]
    df_feature_target = pd.concat([df_feature_target, dfsctarget], axis=1)


    plot_targetspace(df_feature_target, df_metadata, target_region)


    df_feature_target, df_metadata = df_feature_normalize(df_feature_target, df_metadata)
    for key in df_metadata.keys():
        print(key, df_metadata[key])

    result_dic_all = all_regression(df_feature_target, df_metadata["normalized_feature_columns"], target_list)
    print(result_dic_all)

    return df_feature_target, df_metadata
